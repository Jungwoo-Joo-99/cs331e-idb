import os
import sys
import unittest
from models import app, db, Driver, Track, Competed, Driverart, Trackart, Organization, Orgart
db.drop_all()
db.create_all()

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        driver = Driver(idDriver='1')
        db.session.add(driver)
        db.session.commit()
        
        r = db.session.query(Driver).filter_by(idDriver='1').one()
        self.assertEqual(str(r.idDriver), '1')
        db.session.query(Driver).filter_by(idDriver='1').delete()
        db.session.commit()

class Driver_TestCases(unittest.TestCase):
    
    def test_1(self):
        val = Organization(idTeam=22, name='Redbull', year_formed='2005', manager='Tom', location='Austin', country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver = 999,idTeam=22,name = 'Bob',gender='male',nationality='USA',description='Test Info',birthday='01/01/1990',birth_loc='Texas')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Driver).filter_by(idDriver=999).one()
        self.assertEqual(int(r.idDriver),999)

        db.session.query(Driver).filter_by(idDriver=999).delete()
        db.session.query(Organization).filter_by(idTeam=22).delete()
        db.session.commit()

    def test_2(self):
        val = Organization(idTeam=23, name='Redbull', year_formed='2005', manager='Tom', location='Austin',country='USA', description='Test Info 2')
        db.session.add(val)
        
        val = Driver(idDriver = 44,idTeam = 23,name = 'Kris',gender='female',nationality='England',description='Test Info 2',birthday='01/01/1998',birth_loc='Bristol')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Driver).filter_by(idTeam=23).one()
        self.assertEqual(int(r.idTeam),23)

        db.session.query(Driver).filter_by(idDriver=44).delete()
        db.session.query(Organization).filter_by(idTeam=23).delete()
        db.session.commit()

    def test_3(self):
        val = Organization(idTeam=26, name='Redbull', year_formed='2005', manager='Tom', location='Austin', country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver = 91,idTeam = 26,name = 'Ryu',gender='male',nationality='Japan',description='Test Info 3',birthday='01/01/1991',birth_loc='Tokyo')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Driver).filter_by(name='Ryu').one()
        self.assertEqual(str(r.name),'Ryu')

        db.session.query(Driver).filter_by(idDriver=91).delete()
        db.session.query(Organization).filter_by(idTeam=26).delete()
        db.session.commit()
        

class Organization_TestCases(unittest.TestCase):
    def test_1(self):
        val = Organization(idTeam=1,name='Benz',year_formed='1991',manager='Merc Benz',location='Berlin',country='GER',description='Test Info 1')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Organization).filter_by(idTeam=1).one()
        self.assertEqual(str(r.year_formed),'1991')

        db.session.query(Organization).filter_by(idTeam=1).delete()
        db.session.commit()

    def test_2(self):
        val = Organization(idTeam=997,name='Steelers',year_formed='2005',manager='Tom',location='Austin',country='USA',description='Test Info 2')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Organization).filter_by(name='Steelers').one()
        self.assertEqual(int(r.idTeam),997)

        db.session.query(Organization).filter_by(idTeam=997).delete()
        db.session.commit()

    def test_3(self):
        val = Organization(idTeam=17,name='Ford',year_formed='1998',manager='George',location='San Diego',country='USA',description='Test Info 3')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Organization).filter_by(idTeam=17).one()
        self.assertEqual(str(r.name),'Ford')

        db.session.query(Organization).filter_by(idTeam=17).delete()
        db.session.commit()


class Track_TestCases(unittest.TestCase):
    def test_1(self):
        val = Track(idTrack=899,name='Track 1',date_last_raced='01/02/94',video='link 1',country='USA',city='Austin',laps_in_race=5)
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Track).filter_by(idTrack=899).one()
        self.assertEqual(int(r.idTrack),899)

        db.session.query(Track).filter_by(idTrack=899).delete()
        db.session.commit() 

    def test_2(self):
        val = Track(idTrack=31,name='Track 2',date_last_raced='03/23/84',video='link 2',country='GER',city='Berlin',laps_in_race=13)
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Track).filter_by(name='Track 2').one()
        self.assertEqual(str(r.video),'link 2')

        db.session.query(Track).filter_by(idTrack=31).delete()
        db.session.commit() 

    def test_3(self):
        val = Track(idTrack=12,name='Track 3',date_last_raced='04/24/96',video='link 3',country='UK',city='Bristol',laps_in_race=10)
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Track).filter_by(idTrack=12).one()
        self.assertEqual(str(r.date_last_raced),'04/24/96')

        db.session.query(Track).filter_by(idTrack=12).delete()
        db.session.commit() 

class Competed_TestCases(unittest.TestCase):
    def test_1(self):
        val = Organization(idTeam=4, name='Redbull', year_formed='2005', manager='Tom', location='Austin',
                           country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver=1, idTeam=4, name='Bob', gender='male', nationality='USA', description='Test Info',
                     birthday='01/01/1990', birth_loc='Texas')
        db.session.add(val)
        val = Track(idTrack=1, name='Track 1', date_last_raced='01/02/94', video='link 1', country='USA', city='Austin',
                    laps_in_race=5)
        db.session.add(val)
        val = Competed(idDriver=1,idTrack=1,event='Race 1',result=1)
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Competed).filter_by(idDriver=1).one()
        self.assertEqual(int(r.idDriver),1)

        db.session.query(Competed).filter_by(idDriver=1).delete()
        db.session.query(Driver).filter_by(idDriver=1).delete()
        db.session.query(Organization).filter_by(idTeam=4).delete()
        db.session.query(Track).filter_by(idTrack=1).delete()
        db.session.commit() 
    
    def test_2(self):
        val = Organization(idTeam=18, name='Redbull', year_formed='2005', manager='Tom', location='Austin',
                           country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver=7, idTeam=18, name='Bob', gender='male', nationality='USA', description='Test Info',
                     birthday='01/01/1990', birth_loc='Texas')
        db.session.add(val)
        val = Track(idTrack=4, name='Track 1', date_last_raced='01/02/94', video='link 1', country='USA', city='Austin',
                    laps_in_race=5)
        
        db.session.add(val)
        val = Competed(idDriver=7,idTrack=4,event='Race 2',result=2)
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Competed).filter_by(idDriver=7).one()
        self.assertEqual(int(r.result),2)

        db.session.query(Competed).filter_by(idDriver=7).delete()
        db.session.query(Driver).filter_by(idDriver=7).delete()
        db.session.query(Organization).filter_by(idTeam=18).delete()
        db.session.query(Track).filter_by(idTrack=4).delete()
        db.session.commit() 
    
    def test_3(self):
        val = Organization(idTeam=19, name='Redbull', year_formed='2005', manager='Tom', location='Austin',
                           country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver=3, idTeam=19, name='Bob', gender='male', nationality='USA', description='Test Info',
                     birthday='01/01/1990', birth_loc='Texas')
        db.session.add(val)
        val = Track(idTrack=6, name='Track 1', date_last_raced='01/02/94', video='link 1', country='USA', city='Austin',
                    laps_in_race=5)
        db.session.add(val)
        
        val = Competed(idDriver=3,idTrack=6,event='Race 3',result=3)
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Competed).filter_by(idDriver=3).one()
        self.assertEqual(int(r.idTrack),6)

        db.session.query(Competed).filter_by(idDriver=3).delete()
        db.session.query(Driver).filter_by(idDriver=3).delete()
        db.session.query(Organization).filter_by(idTeam=19).delete()
        db.session.query(Track).filter_by(idTrack=6).delete()
        db.session.commit() 

class Driverart_TestCases(unittest.TestCase):
    def test_1(self):

        val = Organization(idTeam=47, name='Redbull', year_formed='2005', manager='Tom', location='Austin', country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver = 1,idTeam=47,name = 'Bob',gender='male',nationality='USA',description='Test Info',birthday='01/01/1990',birth_loc='Texas')
        db.session.add(val)
        
        val = Driverart(idDriver=1,art='link1')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Driverart).filter_by(idDriver=1).one()
        self.assertEqual(int(r.idDriver),1)

        db.session.query(Driverart).filter_by(idDriver=1).delete()
        db.session.query(Driver).filter_by(idDriver=1).delete()
        db.session.query(Organization).filter_by(idTeam = 47).delete()
        db.session.commit()

    def test_2(self):
        val = Organization(idTeam=998, name='Redbull', year_formed='2005', manager='Tom', location='Austin', country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver = 2,idTeam=998,name = 'Bob',gender='male',nationality='USA',description='Test Info',birthday='01/01/1990',birth_loc='Texas')
        db.session.add(val)
        
        val = Driverart(idDriver=2,art='link2')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Driverart).filter_by(idDriver=2).one()
        self.assertEqual(int(r.idDriver),2)

        db.session.query(Driverart).filter_by(idDriver=2).delete()
        db.session.query(Driver).filter_by(idDriver=2).delete()
        db.session.query(Organization).filter_by(idTeam=998).delete()
        db.session.commit()

    def test_3(self):
        val = Organization(idTeam=998, name='Redbull', year_formed='2005', manager='Tom', location='Austin', country='USA', description='Test Info 2')
        db.session.add(val)
        val = Driver(idDriver = 3,idTeam=998,name = 'Bob',gender='male',nationality='USA',description='Test Info',birthday='01/01/1990',birth_loc='Texas')
        db.session.add(val)
        
        
        val = Driverart(idDriver=3,art='link3')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Driverart).filter_by(idDriver=3).one()
        self.assertEqual(str(r.art),'link3')

        db.session.query(Driverart).filter_by(idDriver=3).delete()
        db.session.query(Driver).filter_by(idDriver=3).delete()
        db.session.query(Organization).filter_by(idTeam=998).delete()
        db.session.commit()

class Trackart_TestCases(unittest.TestCase):
    def test_1(self):
        val = Track(idTrack=1,name='Track 1',date_last_raced='01/02/94',video='link 1',country='USA',city='Austin',laps_in_race=5)
        db.session.add(val)
        val = Trackart(idTrack=1,art='link1')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Trackart).filter_by(idTrack=1).one()
        self.assertEqual(int(r.idTrack),1)

        db.session.query(Trackart).filter_by(idTrack=1).delete()
        db.session.query(Track).filter_by(idTrack=1).delete()
        db.session.commit()

    def test_2(self):
        val = Track(idTrack=2,name='Track 1',date_last_raced='01/02/94',video='link 1',country='USA',city='Austin',laps_in_race=5)
        db.session.add(val)
        val = Trackart(idTrack=2,art='link2')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Trackart).filter_by(idTrack=2).one()
        self.assertEqual(int(r.idTrack),2)

        db.session.query(Trackart).filter_by(idTrack=2).delete()
        db.session.query(Track).filter_by(idTrack=2).delete()
        db.session.commit()

    def test_3(self):
        val = Track(idTrack=3,name='Track 1',date_last_raced='01/02/94',video='link 1',country='USA',city='Austin',laps_in_race=5)
        db.session.add(val)
        val = Trackart(idTrack=3,art='link3')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Trackart).filter_by(idTrack=3).one()
        self.assertEqual(str(r.art),'link3')

        db.session.query(Trackart).filter_by(idTrack=3).delete()
        db.session.query(Track).filter_by(idTrack=3).delete()
        db.session.commit()


class Orgart_TestCases(unittest.TestCase):

    def test_1(self):
        val = Organization(idTeam=1,name='Benz',year_formed='1991',manager='Merc Benz',location='Berlin',country='GER',description='Test Info 1')
        db.session.add(val)
        val = Orgart(idTeam=1,art='link1')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Orgart).filter_by(idTeam=1).one()
        self.assertEqual(int(r.idTeam),1)

        db.session.query(Orgart).filter_by(idTeam=1).delete()
        db.session.query(Organization).filter_by(idTeam=1).delete()
        db.session.commit()

    def test_2(self):
        val = Organization(idTeam=2,name='Benz',year_formed='1991',manager='Merc Benz',location='Berlin',country='GER',description='Test Info 1')
        db.session.add(val)        
        val = Orgart(idTeam=2,art='link2')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Orgart).filter_by(idTeam=2).one()
        self.assertEqual(int(r.idTeam),2)

        db.session.query(Orgart).filter_by(idTeam=2).delete()
        db.session.query(Organization).filter_by(idTeam=2).delete()
        db.session.commit()

    def test_3(self):
        val = Organization(idTeam=3,name='Benz',year_formed='1991',manager='Merc Benz',location='Berlin',country='GER',description='Test Info 1')
        db.session.add(val)
        val = Orgart(idTeam=3,art='link3')
        db.session.add(val)
        db.session.commit()

        r = db.session.query(Orgart).filter_by(idTeam=3).one()
        self.assertEqual(str(r.art),'link3')

        db.session.query(Orgart).filter_by(idTeam=3).delete()
        db.session.query(Organization).filter_by(idTeam=3).delete()
        db.session.commit()
        
        

if __name__ == '__main__':
    db.session.rollback()
    unittest.main()