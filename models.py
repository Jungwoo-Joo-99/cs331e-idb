from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from dotenv import load_dotenv
from flask_cors import CORS
app = Flask(__name__, static_folder="./frontend/build/static", template_folder="./frontend/build")
CORS(app)
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://postgres:admin@localhost:5432/f1db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Driver(db.Model):
    """
    Driver class has six attributes
    idDriver - pk
    idTeam - one to many relation
    name
    nationality
    description
    birthday
    birth_loc
    tracks - many to many relation
    """

    __tablename__ = 'driver'

    idDriver = db.Column(db.Integer, primary_key=True)

    idTeam = db.Column(db.Integer, db.ForeignKey('organization.idTeam'))
    # organization = db.relationship("Organization", backref="driver")

    name = db.Column(db.String(80))
    gender = db.Column(db.String(10))
    nationality = db.Column(db.String(80))
    description = db.Column(db.String(30000))
    tracks = db.relationship("Competed", back_populates='driver')
    birthday = db.Column(db.String(40))
    birth_loc = db.Column(db.String(40))

    driverart = db.relationship(
        "Driverart", uselist=False, back_populates="driver")

    @property
    def serialize(self):
        return {
            'idDriver': self.idDriver,
            'idTeam': self.idTeam,
            # 'organization': self.organization.serialize,
            'name': self.name,
            'nationality': self.nationality,
            'description': self.description,
            'driverart': self.driverart.art,
            'gender': self.gender,
            'tracks': self.serialize_competeds,
            'birthday': self.birthday,
            'birth_loc': self.birth_loc
        }

    @property
    def serialize_competeds(self):
      return [item.serialize for item in self.tracks]
  
class Organization(db.Model):
    """
    Organization has seven attributes
    idTeam - pk
    name
    year_formed
    manager
    location
    country
    description
    drivers - one to many relation
    """
    __tablename__ = 'organization'

    idTeam = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    year_formed = db.Column(db.String(20))
    manager = db.Column(db.String(80))
    location = db.Column(db.String(80))
    country = db.Column(db.String(60))
    description = db.Column(db.String(8000))

    drivers = db.relationship("Driver", backref="organization")
    orgart = db.relationship("Orgart", uselist=False,
                             back_populates="organization")

    @property
    def serialize(self):
        return {
            'idTeam': self.idTeam,
            'name': self.name,
            'year_formed': self.year_formed,
            'manager': self.manager,
            'location': self.location,
            'country': self.country,
            'description': self.description,
            'drivers': self.serialize_drivers,
            'orgart': self.orgart.art
        }

    @property
    def serialize_drivers(self):
        return [item.serialize for item in self.drivers]

class Track(db.Model):
    """
    Track has six attributes
    idTrack - pk
    date_last_raced
    video
    name
    drivers - many to many relation
    country
    city
    laps_in_race
    """
    __tablename__ = 'track'

    idTrack = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    date_last_raced = db.Column(db.String(80))
    video = db.Column(db.String(200))
    drivers = db.relationship("Competed", back_populates='track')
    country = db.Column(db.String(40))
    city = db.Column(db.String(40))
    laps_in_race = db.Column(db.Integer)
    trackart = db.relationship(
        "Trackart", uselist=False, back_populates="track")

    @property
    def serialize(self):
        return {
            'idTrack': self.idTrack,
            'name': self.name,
            'date_last_raced': self.date_last_raced,
            'country': self.country,
            'city': self.city,
            'video': self.video,
            'drivers': self.serialize_competeds,
            'trackart': self.trackart.art
        }

    @property
    def serialize_competeds(self):
      return [item.serialize for item in self.drivers]

class Competed(db.Model):
  __tablename__ = 'competed'
  """
  Competed has two attributes
  idDriver - pk  
  idTrack - pk
  event
  result
  driver - one to many relation
  track - one to many relation
  """
  idDriver = db.Column(db.Integer, db.ForeignKey(
      'driver.idDriver'), primary_key=True)
  idTrack = db.Column(db.Integer, db.ForeignKey(
      'track.idTrack'), primary_key=True)
  event = db.Column(db.String(100))
  result = db.Column(db.Integer)

  driver = db.relationship('Driver', backref=db.backref('track_association'))
  track = db.relationship('Track', backref=db.backref('driver_association'))

  @property
  def serialize(self):
      return {
          'idDriver': self.idDriver,
          'idTrack': self.idTrack,
          'event': self.event,
          'result': self.result,
          'driver': self.driver.name,
          'track': self.track.name
        }

class Driverart(db.Model):
  """
  Driverart has two attributes
  idDriver - pk
  art
  driver - one to one relation
  """
  __tablename__ = 'driverart'
  idDriver = db.Column(db.Integer, db.ForeignKey(
      'driver.idDriver'), primary_key=True)
  art = db.Column(db.String(4000), primary_key=True)

  driver = db.relationship("Driver", back_populates="driverart")

  @property
  def serialize(self):
    return {
      'idDriver': self.idDriver,
      'art': self.art,
      'driver': self.driver.serialize
    }
  
class Trackart(db.Model):
  """
  Trackart has two attributes
  idTrack - pk
  art
  track - one to one relation
  """
  __tablename__ = 'trackart'
  idTrack = db.Column(db.Integer, db.ForeignKey(
      'track.idTrack'), primary_key=True)
  art = db.Column(db.String(400), primary_key=True)

  track = db.relationship("Track", back_populates="trackart")

  @property
  def serialize(self):
    return {
      'idTrack': self.idTrack,
      'art': self.art,
      'track': self.track.serialize
    }

class Orgart(db.Model):
  """
  Orgart has two attributes
  idTeam - pk
  art
  organization - one to one relation
  """
  __tablename__ = 'orgart'

  idTeam = db.Column(db.Integer, db.ForeignKey(
      'organization.idTeam'), primary_key=True)
  art = db.Column(db.String(4000), primary_key=True)
  organization = db.relationship("Organization", back_populates="orgart")

  @property
  def serialize(self):
      return {
        'idTeam': self.idTeam,
        'art': self.art,
        'organization': self.organization.serialize
      }

class Issue(db.Model):
    '''
    Issue has one attribute
    idIssue
    user
    '''
    __tablename__ = 'issues'
    idIssue = db.Column(db.Integer, primary_key = True)
    user = db.Column(db.String(40))

    @property
    def serialize(self):
        return{
            'idIssue': self.idIssue,
            'user': self.user
        }

class Commit(db.Model):
    '''
    Commit has one attribute
    idShort
    user
    '''
    __tablename__ = 'commits'
    idShort = db.Column(db.String(8), primary_key = True)
    user = db.Column(db.String(40))

    @property
    def serialize(self):
        return{
            'idShort': self.idShort,
            'user': self.user
        }

class Unit(db.Model):
    '''
    Unit has two attributes
    idUnit - pk
    name
    count
    '''
    __tablename__ = 'units'
    idUnit = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(40))
    count = db.Column(db.Integer)

    @property
    def serialize(self):
        return{
            'idUnit': self.idUnit,
            'name': self.name,
            'count': self.count
        }


#db.drop_all()
#db.create_all()

