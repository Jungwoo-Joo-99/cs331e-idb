import React from 'react';
import logo from '../logo.png';
import max from '../images/max_v.png';
import track from '../images/abu_dhabi.png';
import team from '../images/mercedes.png';
import '../style/style.css';
import { Container, Row, Col, Button, Card, CardDeck } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { HashLink } from 'react-router-hash-link';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';

export default function Home() {
  return (
    <div className="home_content">
      <Container>
        <Row className="main_row">
          <Col md={6} className="home_bio">
            <h1 id="home_title">Donut Racing</h1>
            <p>Donut Racing is a one-stop site where you can get the latest information you need to know about Formula One. Here you can discover more about your favorite F1 drivers, tracks, and teams.</p>
            <br></br>
            <HashLink to="#pillars">
              <Button variant="primary" renderAs="button">Learn More	&darr;</Button>
            </HashLink>
          </Col>
          <Col md={6}>
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="75%" />
          </Col>
        </Row>
      </Container>

      <Container id="pillars">
        <Row className="pillars_row justify-content-center">
          <CardDeck>
            <Card className="border-0" style={{ width: '18rem' }}>
              <Card.Img variant="top" src={max} alt="Max Verstappen" />
              <Card.Body>
                <Card.Title class="card-title">Drivers</Card.Title>
                <Card.Text>
                  From where F1 drivers are born to the team they currently race for, you can find it here.
                </Card.Text>
                <Link to="/drivers">
                  <Button variant="primary" renderAs="button">See More</Button>
                </Link>
              </Card.Body>
            </Card>

            <Card className="border-0" style={{ width: '18rem' }}>
              <Card.Img variant="top" src={track} />
              <Card.Body>
                <Card.Title class="card-title">Tracks</Card.Title>
                <Card.Text>
                  Find out more about the tracks your favorite races have been last held at and when.
                </Card.Text>
                <Link to="/tracks">
                  <Button variant="primary" renderAs="button">See More</Button>
                </Link>
              </Card.Body>
            </Card>

            <Card className="border-0" style={{ width: '18rem' }}>
              <Card.Img variant="top" src={team} />
              <Card.Body>
                <Card.Title class="card-title">Teams</Card.Title>
                <Card.Text>
                  Catch up on the key information you need to know about Formula One's teams.
                </Card.Text>
                <Link to="/teams">
                  <Button variant="primary" renderAs="button">See More</Button>
                </Link>
              </Card.Body>
            </Card>
          </CardDeck>
        </Row>
      </Container>
    </div>
  );
}