import useAxios from "axios-hooks";
import React, { useState, useEffect } from "react";
import { CardDeck, ListGroup, ListGroupItem, Card, Button, ToggleButtonGroup, Pagination, Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, useParams, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/custom.css';
import { useBootstrapPrefix } from "react-bootstrap/esm/ThemeProvider";
import logo from '../logo.png';
/* const tracks = [{'id':0, 'name':'Abu Dhabi Grand Prix','loc':'Abu Dhabi, UAE','date':'12/13/20','circuit':'Yas Marina Circuit', 'competitors': [{'name':'Max Verstappen', 'id':0}, {'name':'Valtteri Bottas', 'id':1}, {'name':'Lando Norris', 'id':2}], 'image': 'https://www.thesportsdb.com/images/media/event/thumb/s22twe1610814532.jpg'},
{'id':1, 'name':'Bahrain Grand Prix','loc':'Sakhir, Bahrain','date':'11/29/20','circuit':'Bahrain International Circuit', 'competitors': [{'name':'Max Verstappen', 'id':0}, {'name':'Valtteri Bottas', 'id':1}, {'name':'Lando Norris', 'id':2}], 'image': 'https://www.thesportsdb.com/images/media/event/thumb/8l6v3s1610814042.jpg'},
{'id':2, 'name':'Turkish Grand Prix','loc':'Istanbul, Turkey','date':'11/15/20','circuit':'Intercity Istanbul Park', 'competitors': [{'name':'Max Verstappen', 'id':0}, {'name':'Valtteri Bottas', 'id':1}, {'name':'Lando Norris', 'id':2}], 'image': 'https://www.thesportsdb.com/images/media/event/thumb/ty76f41605469172.jpg'}] */
function Tracks() {
    const [asc, setOrder] = useState(true);
    const [page, setPage] = useState(1);
    const [sortType, setSortType] = useState('name');
    const [{ data: getTracks, loading: getTLoading, error: getTError }] = useAxios("/api/tracks");
    const [list, setList] = useState([]);
    let pages = [];
    if (getTracks != null) {
        var len = getTracks.length / 6;
        if (getTracks.length % 6 != 0) {
            len += 1;
        }
        for (let num = 1; num <= len; num++) {
            pages.push(
                <Pagination.Item key={num} active={num === page} onClick={() => setPage(num)}>
                    {num}
                </Pagination.Item>
            )
        }
    }
    useEffect(() => {
        async function sortArray(type){
            const types = {
                name: 'name',
                date_last_raced: 'date_last_raced',
                country: 'country',
                city: 'city'
            };
            const sortProperty = types[type];
            const sorted = [...getTracks].sort((a, b) => {
                if (!asc) {
                    var temp = b;
                    b = a;
                    a = temp;
                }
                if (b[sortProperty] > a[sortProperty]) {
                    return -1;
                }
                else if (b[sortProperty] < a[sortProperty]) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
            await setList(sorted.slice((page - 1) * 6, page * 6));
        };
        if (getTracks != null){
            sortArray(sortType);
        }
    }, [sortType, getTracks, page, asc]);
    if (getTLoading){
        return (<Container className="home_content">
            <img src={logo} alt="Donut logo with racetrack icing" className="App-logo" width="40%" />
            <h1>Loading...</h1>
        </Container>);
    }
    if (getTError){
        return <p>Error</p>
    }

    return (
        <div>
            <Container className="about_proj home_content">
            <h1>F1 Tracks</h1>
            <Card class="col-md-12 text-center">
                <ToggleButtonGroup type="radio" name='sortOptions' class="row">
                    <Button class="col" variant="secondary" onClick={() => setSortType('name')} value='name'>
                        Name
                    </Button>
                    <Button class="col" variant="secondary" onClick={() => setSortType('date_last_raced')} value='date_last_raced'>
                        Date Last Raced
                    </Button>
                    <Button class="col" variant="secondary" onClick={() => setSortType('country')} value='country'>
                        Country
                    </Button>
                    <Button class="col" variant="secondary" onClick={() => setSortType('city')} value='city'>
                        City
                    </Button>
                    <Button class="col" variant="secondary" onClick={() => {
                        setOrder(!asc);
                    }} value='reverse'>
                        Reverse Order
                    </Button>
                </ToggleButtonGroup>
            </Card>
            <br></br>
            <CardDeck class="row col-xs justify-content-center" id="cards">
                {list.map(track => (
                    <Card className="col-sm-3 gx-3 m-3" style={{ width: 20 + "rem" }} >
                        <Card.Img variant="top" src={track.trackart} alt={track.name} />
                        <Card.Body>
                            <Card.Title class="card-title">{track.name}</Card.Title>
                        </Card.Body>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem>
                                <strong>Last Raced:</strong> {track.date_last_raced}
                            </ListGroupItem>
                            <ListGroupItem>
                                <strong>Located:</strong> {track.city}, {track.country}
                            </ListGroupItem>
                            <ListGroupItem>
                                <Button class="about-btn" variant="primary" renderAs="button" href={'/track/'+track.idTrack} block>Learn More</Button>
                            </ListGroupItem>
                        </ListGroup>
                    </Card>
                )
                )}
                </CardDeck>
                <Pagination class="text-center">{pages}</Pagination>
            </Container>
        </div>
    );
}
export default Tracks;