import json
from models import app, db, Driver, Organization, Track, Competed, Orgart, Driverart, Trackart, Issue, Commit, Unit

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename,errors='ignore') as file:
        jsn = json.load(file)
        file.close()

    return jsn
db.drop_all()
db.create_all()

# ------------
# create_Organization
# ------------
def create_Organization():
    """
    populate organization table
    """
    organization = load_json('organizations.json')

    for team in organization['teams']:
        idTeam = team['idTeam']
        name = team['strTeam']
        year_formed = team['intFormedYear']
        manager = team['strManager']
        location = team['strStadiumLocation']
        country = team['strCountry']
        description = team['strDescriptionEN']
        newOrg = Organization(idTeam=idTeam,name=name,year_formed=year_formed,manager=manager,location=location,country=country,description=description)
        
        # After I create the book, I can then add it to my session. 
        db.session.add(newOrg)
        # commit the session to my DB.
        db.session.commit()
	
create_Organization()

def create_Drivers():
    driver = load_json('drivers.json')

    for player in driver['player']:
        if player['strPosition'] == 'Manager':
            continue
        idDriver = player['idPlayer']
        idTeam = player['idTeam']
        name = player['strPlayer']
        gender = player['strGender']
        nationality = player['strNationality']
        description = player['strDescriptionEN']
        birthday = player['dateBorn']
        birth_loc = player['strBirthLocation']
        
        newDriver = Driver(idDriver=idDriver,idTeam=idTeam,name=name,gender=gender,nationality=nationality,description=description,birthday=birthday,birth_loc=birth_loc)

        db.session.add(newDriver)
        db.session.commit()

create_Drivers()

def create_Tracks():
    track = load_json('events.json')

    for course in track['events']:
        idTrack = course['idEvent']
        date_last_raced = course['dateEvent']
        video = course['strVideo']
        name = course['strVenue']
        country = course['strCountry']
        city = course['strCity']
        laps_in_race = course['intRound']

        newTrack = Track(idTrack=idTrack,date_last_raced=date_last_raced,video=video,name=name, country=country,city=city,laps_in_race=laps_in_race)
        db.session.add(newTrack)
        db.session.commit()
create_Tracks()
        
# ----------
#create_Orgart
#-----------
def create_Orgart():
    '''
    populate orgart table
    '''
    orgart = load_json('organizations.json')

    for team in orgart['teams']:
        idTeam = team['idTeam']
        #arts = [team['strTeamBadge'],team['strTeamJersey'],team['strTeamLogo'],team['strTeamFanart1'],team['strTeamFanart2'],team['strTeamFanart3'],team['strTeamFanart4'],team['strTeamBanner']]
        art = team['strTeamBadge']
        #for things in arts:
        #    if things != None:
        #        art += (things+',')
        #art = art[:-1]
        newOrgart = Orgart(idTeam=idTeam,art=art)
        # After I create the book, I can then add it to my session. 
        db.session.add(newOrgart)
        # commit the session to my DB.
        db.session.commit()
create_Orgart()

def create_Driverart():

    driverart= load_json('drivers.json')

    for player in driverart['player']:
        if player['strPosition'] == 'Manager':
            continue
        idDriver = player['idPlayer']
        #arts = [player['strThumb'],player['strCutout'],player['strBanner'],player['strFanart1'],player['strFanart2'],player['strFanart3'],player['strFanart4']]
        art = player['strThumb']
        #for things in arts:
        #    if things != None:
        #        art += (things+',')
        #art = art[:-1]
        newDriverart = Driverart(idDriver=idDriver,art=art)
        db.session.add(newDriverart)
        db.session.commit()
create_Driverart()

def create_Trackart():
    trackart = load_json('events.json')
    for course in trackart['events']:
        idTrack = course['idEvent']
        #arts = [course['strPoster'],course['strFanart'],course['strThumb'],course['strBanner'],course['strMap']]
        art = course['strMap']
        #for things in arts:
        #    if things != None:
        #        art += (things+',')
        #art = art[:-1]
        newTrackart = Trackart(idTrack=idTrack,art=art)
        db.session.add(newTrackart)
        db.session.commit()
create_Trackart()

def create_Competed():
    competed = load_json('results.json')
    for results in competed['results']:
        idDriver = results['idPlayer']
        idTrack = results['idEvent']
        event = results['strEvent']
        result = results['intPosition']

        newCompeted = Competed(idDriver=idDriver,idTrack=idTrack,event=event,result=result)
        db.session.add(newCompeted)
        db.session.commit()
create_Competed()

# ----------
#GitLab About Statistics
#-----------

def create_Issues():
    issues = load_json('issues.json')
    for issue in issues:
        idIssue = issue['id']
        user = issue['author']['username']

        newIssue = Issue(idIssue = idIssue, user = user)
        db.session.add(newIssue)
        db.session.commit()
create_Issues()

def create_Commits():
    commits = load_json('commits.json')
    for commit in commits:
        idShort = commit['short_id']
        user = commit['author_name']

        newCommit = Commit(idShort = idShort, user = user)
        db.session.add(newCommit)
        db.session.commit()
create_Commits()

def create_Units():
    units = load_json('unit_tests.json')
    for unit in units:
        idUnit = unit['id']
        name = unit['name']
        count = unit['count']

        newUnit = Unit(idUnit = idUnit, name = name, count = count)
        db.session.add(newUnit)
        db.session.commit()
create_Units()